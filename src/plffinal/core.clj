(ns plffinal.core
  (:gen-class)
  (:require [plffinal.coord :as c]))



;rellenar un vector con un hashmap de caracteres
;(defn listaCTest
;  [a]
; (let [b (fn [a] (into [] (hash-map char a)))]
;  (b a)))


;rellenar un vector vacio con map mejor
;(defn vectorCT
 ; [a]
 ;(let [b (fn [a] (into [] (map char a)))]
  ;(b a)))


;meter en el vector un map y concadenar 
(defn vectorCT
  [& args]
  (into [] (map char (apply str (concat args)))))

;remplazar los caracteres del vectorCT con lo que definimos en las coordenada 
(defn equivalente
  [a]
  (let [b (fn [x] (map c/coordenadas (vectorCT x)))]
    (b a)))


;(defn convertir-a-coordenada
 ; [a]
  ;(let [b (fn [x] (map c/coordenadas (equivalente x)))]
   ; (b a)))

(defn caracteres-count
  [x]
  (let [y (fn [p] (= p \<))]
    (count (filterv y (map char (vectorCT x))))))


(defn puntos-card
  [s]
  (let [
        ↑ (fn [p] (= p \^))
        ↓ (fn [p] (= p \v))
        ← (fn [p] (= p \<))
        → (fn [p] (= p \>))
        norte (count (filter ↑ (map char (vectorCT s))))
        este (count (filter → (map char (vectorCT s))))
        sur (count (filter ↓ (map char (vectorCT s))))
        oeste (count (filter ← (map char (vectorCT s))))
        ]

    {\^ norte \v sur \> este \< oeste}))



(defn filtro
  [pred & args]
  (count (filter pred (map char (vectorCT (into [] args))))))


(defn puntos-card-filtro
   ([] [])
  ([k & args]

  (let [↑ (fn [p] (= p \^))
        ↓ (fn [p] (= p \v))
        ← (fn [p] (= p \<))
        → (fn [p] (= p \>))
      
         norte (filtro ↑ (into [] args))
         sur (filtro  ↓ (into [] args))
         este (filtro ← (into [] args))
         oeste (filtro → (into [] args))
        
        ]

    {\^ norte \v sur \> este \< oeste} k)))

















